# AI with neural network plays against rantom player
# if AI wins it gains +1 point, if there is a draw 0 points
# AI gains -2 points in case of loss
NNvsRandomPlayerGame <- function(tic.ai){
  # AI side
  aiSide <- sample(c(-1,1),1)
  
  # side which starts the game
  side <- sample(c(-1,1),1)
  
  board <- GenerateEmptyBoard() 
  eval <- 0
  
  # play a game
  while(eval == 0 && IsMovePossible(board)){
    if(side==aiSide){
      board <- Move(tic.ai, board, side)
    } else {
      # Make a valid move completely at random
      move <- sample(which(board==0),1)
      board[[move]] <- side
    }
    eval <- EvaluateBoard(board)
    side <- side * -1
  }
  
  # return result
  if((eval > 0 && 1 == aiSide) || (eval < 0 && -1 == aiSide)){
    return(3)
  }
  if(eval == 0){
    return(2)
  }
  return(0)
}

# AI with neural network plays against another one
# if first one wins it gains +1 point, if there is a draw 0 points
# AI gains -2 points in case of loss
NNvsNNGame <- function(first, second){
  # side of first player
  firstPlayerSide <- sample(c(-1,1),1)
  
  # side which starts the game
  side <- sample(c(-1,1),1)
  
  board <- GenerateEmptyBoard() 
  eval <- 0
  
  # play a game
  while(eval == 0 && IsMovePossible(board)){
    if(side==firstPlayerSide){
      board <- Move(first, board, side)
    } else {
      board <- Move(second, board, side)
    }
    eval <- EvaluateBoard(board)
    side <- side * -1
  }
  
  # return result
  if(eval == firstPlayerSide) return(3)
  if(eval == 0) return(2)
  return(0)
}

NNvsNN2Games <- function(first, second){
  # side of first player
  firstPlayerSide <- 1
  
  # side which starts the game
  side <- 1
  
  board <- GenerateEmptyBoard() 
  eval <- 0
  
  # play a game
  while(eval == 0 && IsMovePossible(board)){
    if(side==firstPlayerSide){
      board <- Move(first, board, side)
    } else {
      board <- Move(second, board, side)
    }
    eval <- EvaluateBoard(board)
    side <- side * -1
  }
  
  side <- -1
  board <- GenerateEmptyBoard() 
  eval2 <- 0
  
  # play a game
  while(eval2 == 0 && IsMovePossible(board)){
    if(side==firstPlayerSide){
      board <- Move(first, board, side)
    } else {
      board <- Move(second, board, side)
    }
    eval2 <- EvaluateBoard(board)
    side <- side * -1
  }
  
  # return result
  if ((eval > 0 && eval2 >= 0) || (eval >= 0 && eval2 > 0)) return(3)
  if ((eval < 0 && eval2 <= 0) || (eval <=0 && eval2 < 0)) return(0)
  return(2)
}

PlayWithMe <- function(tic.ai){
   # AI side
    aiSide <- sample(c(-1,1),1)

    # side which starts the game
    side <- sample(c(-1,1),1)

    board <- GenerateEmptyBoard() 
    eval <- 0

    # play a game
    while(eval == 0 && IsMovePossible(board)){
      if(side==aiSide){
        board <- Move(tic.ai, board, side)
      } else {
        print(DisplayBoard(board))
        move <- readinteger()
        board[[move]] <- side
      }
      eval <- EvaluateBoard(board)
      side <- side * -1
    }

    # return result
    cat("Player side = ", (-1*aiSide), "\n")
    if(eval == aiSide) return(0)
    if(eval == 0) return(2)
    return(3)
}

PlayWithMe2Games <- function(tic.ai){
  firstPlayerSide <- 1
  
  # side which starts the game
  side <- 1
  board <- GenerateEmptyBoard() 
  eval <- 0
  
  cat("Player side = ", (firstPlayerSide), "\n")

  # play a game
  while(eval == 0 && IsMovePossible(board)){
    if(side==firstPlayerSide){
      board <- Move(tic.ai, board, side)
    } else {
        print(DisplayBoard(board))
        move <- readinteger()
        board[[move]] <- side
    }
    eval <- EvaluateBoard(board)
    side <- side * -1
  }

  cat("Player score = ", eval, "\n")
  
  side <- -1
  board <- GenerateEmptyBoard() 
  eval2 <- 0
  
  # play a game
  while(eval2 == 0 && IsMovePossible(board)){
    if(side==firstPlayerSide){
      board <- Move(tic.ai, board, side)
    } else {
        print(DisplayBoard(board))
        move <- readinteger()
        board[[move]] <- side
    }
    eval2 <- EvaluateBoard(board)
    side <- side * -1
  }

  cat("Player score = ", eval2, "\n")
  
  # return result
  if(eval+eval2 > 0) return(3)
  if(eval+eval2 == 0) return(2)
  return(0)
}